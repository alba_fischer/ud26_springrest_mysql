CREATE DATABASE IF NOT EXISTS `piezas_y_proveedores`;
USE `piezas_y_proveedores`;

DROP TABLE IF EXISTS `suministra`;
DROP TABLE IF EXISTS `proveedores`;
DROP TABLE IF EXISTS `piezas`;

CREATE TABLE `piezas`(
	`id` INT NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(100),
    PRIMARY KEY (`id`)
);

INSERT INTO `piezas` (nombre) VALUES ('tornillo'), ('taladro'), ('martillo'), ('tuerca'), ('destornillador');

CREATE TABLE `proveedores`(
	`id` CHAR(4) PRIMARY KEY,
    `nombre` VARCHAR(100)
);

INSERT INTO `proveedores` VALUES ('1234', 'CPM'), ('0023', 'SIRA'), ('1256', 'Montsistem'), ('9876', 'New Tools');

CREATE TABLE `suministra` (
    `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `pieza_id` INT,
    `proveedor_id` CHAR(4),
    `precio` INT,
    KEY (`pieza_id`, `proveedor_id`),
    FOREIGN KEY (`pieza_id`) REFERENCES `piezas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (`proveedor_id`) REFERENCES `proveedores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO `suministra` (pieza_id, proveedor_id, precio) VALUES (1, '1234', 3), (2, '0023', 11), (1, '9876', 20), (4, '1256', 75);

