package es.http.service.service;

import java.util.List;

import es.http.service.dto.Proveedor;



public interface IProveedorService {

	//Metodos del CRUD
		public List<Proveedor> listarProveedores(); //Listar all 
		
		public Proveedor guardarProveedor(Proveedor proveedor);	//Guarda una proveedor CREATE
		
		public Proveedor proveedorXID(int id); //Leer datos de una proveedor READ
		
		public Proveedor actualizarProveedor(Proveedor proveedor); //Actualiza datos de la proveedor UPDATE
		
		public void eliminarProveedor(int id);// Elimina la proveedor DELETE
	

}



