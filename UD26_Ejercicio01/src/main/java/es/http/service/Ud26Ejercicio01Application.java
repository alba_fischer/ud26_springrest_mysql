package es.http.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ud26Ejercicio01Application {

	public static void main(String[] args) {
		SpringApplication.run(Ud26Ejercicio01Application.class, args);
	}

}
