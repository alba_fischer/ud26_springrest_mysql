package com.example.demo.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;



@Entity
@Table(name="cientificos")
public class Cientifico {

	//Atributos de entidad cientifico
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "dni")
	private String dni;
	@Column(name = "nom_apels")
	private String nom_apels;
	
	@OneToMany
    @JoinColumn(name="id")
    private List<Asignado> asignado;
	
	//Constructores
	
	public Cientifico() {
	
	}

	/**
	 * @param dni
	 * @param nom_apels
	 * @param Asignado
	 */
	public Cientifico(String dni, String nom_apels, List<Asignado> asignado) {
		this.dni = dni;
		this.nom_apels = nom_apels;
		this.asignado = asignado;
	}

	/**
	 * @return the dni
	 */
	public String getDni() {
		return dni;
	}

	/**
	 * @param dni the dni to set
	 */
	public void setDni(String dni) {
		this.dni = dni;
	}

	/**
	 * @return the nom_apels
	 */
	public String getNom_apels() {
		return nom_apels;
	}

	/**
	 * @param nom_apels the nom_apels to set
	 */
	public void setNom_apels(String nom_apels) {
		this.nom_apels = nom_apels;
	}



	/**
	 * @return the asignado
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "Asignado")
	public List<Asignado> getAsignado() {
		return asignado;
	}

	/**
	 * @param asignado the asignado to set
	 */
	public void setAsignado(List<Asignado> asignado) {
		this.asignado = asignado;
	}

	@Override
	public String toString() {
		return "Cientifico [dni=" + dni + ", nom_apels=" + nom_apels +  "]";
	}


	
	
	
}
