package com.example.demo.dto;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="asignado")
public class Asignado {

	//Atributos de entidad asignado_a
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_asign")
	private int id;
	
	@ManyToOne
    @JoinColumn(name = "cientifico_id")
    Cientifico cientifico_id;
 
    @ManyToOne
    @JoinColumn(name = "proyecto_id")
    Proyecto proyecto_id;
	
	
	//Constructores
	
	public Asignado() {
	
	}

	public Asignado(int id, Cientifico cientifico_id, Proyecto proyecto_id) {
		this.id = id;
		this.cientifico_id = cientifico_id;
		this.proyecto_id = proyecto_id;
	}


	//Getters y Setters
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}


	/**
	 * @return the cientifico_id
	 */
	public Cientifico getCientifico_id() {
		return cientifico_id;
	}


	/**
	 * @param cientifico_id the cientifico_id to set
	 */
	public void setCientifico_id(Cientifico cientifico_id) {
		this.cientifico_id = cientifico_id;
	}


	/**
	 * @return the proyecto_id
	 */
	public Proyecto getProyecto_id() {
		return proyecto_id;
	}


	/**
	 * @param proyecto_id the proyecto_id to set
	 */
	public void setProyecto_id(Proyecto proyecto_id) {
		this.proyecto_id = proyecto_id;
	}


	
	//Metodo impresion de datos por consola
	@Override
	public String toString() {
		return "RegistroCurso [id=" + id + ", cientifico_id=" + cientifico_id + ", proyecto_id=" + proyecto_id + "]";
	}

	
	
	
	
	




		
	
	
	
}
