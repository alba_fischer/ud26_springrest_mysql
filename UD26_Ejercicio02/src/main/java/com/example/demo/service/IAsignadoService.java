package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.Asignado;

public interface IAsignadoService {

	//Metodos del CRUD
		public List<Asignado> listarAsignado(); //Listar All 
		
		public Asignado guardarAsignado(Asignado asignado);	//Guarda una asignación CREATE
		
		public Asignado asignadoXID(int id); //Leer datos de una asignación READ
		
		public Asignado actualizarAsignado(Asignado asignado); //Actualiza datos de la asignación UPDATE
		
		public void eliminarAsignado(int id);// Elimina la asignación DELETE
	
}

