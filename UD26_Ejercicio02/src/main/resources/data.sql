CREATE DATABASE IF NOT EXISTS `los_cientificos`;
USE `los_cientificos`;

DROP TABLE IF EXISTS `asignado`;
DROP TABLE IF EXISTS `proyecto`;
DROP TABLE IF EXISTS `cientificos`;


CREATE TABLE `cientificos` (
  `dni` varchar(8) NOT NULL DEFAULT '',
  `nom_apels` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`dni`)
);

INSERT INTO `cientificos` (dni,nom_apels) VALUES 
('48752136', 'Alba Gras'),
('85967253', 'Raquel Balada'),
('78549623', 'Berta Gras');


CREATE TABLE `proyecto` (
  `id` char(4) NOT NULL DEFAULT '',
  `nombre` varchar(250) DEFAULT NULL,
  `horas` int DEFAULT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `proyecto` (id,nombre,horas) VALUES 
('1234', 'Fluidos',300),
('4321', 'Termodinámica',298),
('5678', 'Electromagnetismo',345);

CREATE TABLE `asignado` (
  `id_asign` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `cientifico_id` varchar(8) NOT NULL DEFAULT '',
  `proyecto_id` char(4) NOT NULL DEFAULT '',
  KEY (`cientifico_id`,`proyecto_id`),
  FOREIGN KEY (`cientifico_id`) REFERENCES `cientificos` (`dni`)  ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`)  ON DELETE CASCADE ON UPDATE CASCADE

);

INSERT INTO `asignado` (cientifico_id, proyecto_id) VALUES 
('48752136', '1234'), 
('85967253', '1234');

