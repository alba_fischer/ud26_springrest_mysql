CREATE DATABASE IF NOT EXISTS `los_investigadores`;
USE `los_investigadores`;

DROP TABLE IF EXISTS `reserva`;
DROP TABLE IF EXISTS `investigadores`;
DROP TABLE IF EXISTS `equipos`;
DROP TABLE IF EXISTS `facultad`;

CREATE TABLE facultad(
    id INT AUTO_INCREMENT PRIMARY KEY,
    nombre NVARCHAR(100)
);

INSERT INTO facultad (nombre) VALUES ('Facultad de Física UB'), ('Facultad de Química UB'), ('Facultad de Informàtica UPC');


CREATE TABLE equipos(
	num_serie CHAR(4) PRIMARY KEY,
    nombre NVARCHAR(100),
    facultad INT,
    FOREIGN KEY (facultad) REFERENCES facultad(id)
);

INSERT INTO equipos VALUES ('1', 'Equipo 1', 1), ('2', 'Equipo 2', 2), ('3', 'Equipo 3', 3);


CREATE TABLE investigadores(
	dni VARCHAR(8) PRIMARY KEY,
    nom_apels NVARCHAR(255),
    facultad INT,
    FOREIGN KEY (facultad) REFERENCES facultad(id)
);

INSERT INTO investigadores VALUES ('58963251', 'Esther Carles Gras', 1), ('78596412', 'Aleix Cartoixa Balada', 2), ('96854120', 'Alba Martínez Roca', 3);


CREATE TABLE reserva(
	id INT AUTO_INCREMENT PRIMARY KEY,
	dni VARCHAR(8),
	num_serie CHAR(4),
	comienzo DATE,
    fin DATE,
	FOREIGN KEY (dni) REFERENCES investigadores(dni),
    FOREIGN KEY (num_serie) REFERENCES equipos(num_serie)
);

INSERT INTO reserva (dni, num_serie, comienzo, fin) VALUES ('58963251', '1', '2017-3-7', '2019-07-8'), ('78596412', '2', '2018-02-19', '2019-06-4'), ('96854120', '3', '2016-05-15', '2020-01-25');

