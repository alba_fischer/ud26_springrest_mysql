package com.example.demo.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.demo.dto.Investigador;
import com.example.demo.service.InvestigadorServiceImpl;

@RestController
@RequestMapping("/api")
public class InvestigadorController {

	@Autowired
	InvestigadorServiceImpl investigadorServiceImpl;
	
	@GetMapping("/investigadores")
	public List<Investigador> listarInvestigadores(){
		return investigadorServiceImpl.listarInvestigadores();
	}
	
	@PostMapping("/investigadores")
	public Investigador salvarInvestigador(@RequestBody Investigador investigador) {
		return investigadorServiceImpl.guardarInvestigador(investigador);
	}
	
	@GetMapping("/investigadores/{id}")
	public Investigador investigadorXID(@PathVariable(name="id") String id) {
		Investigador investigadorXId = new Investigador();
		
		investigadorXId = investigadorServiceImpl.investigadorXID(id);
		
		System.out.println("Investigador XID: " + investigadorXId);
		
		return investigadorXId;
	}
	
	@PutMapping("/investigadores/{id}")
	public Investigador actualizarInvestigador(@PathVariable(name="id") String id, @RequestBody Investigador investigador) {
		Investigador investigadorSeleccionado = new Investigador();
		Investigador investigadorActualizado = new Investigador();
		
		investigadorSeleccionado = investigadorServiceImpl.investigadorXID(id);
		
		investigadorSeleccionado.setNombre(investigador.getNombre());
		investigadorSeleccionado.setFacultad(investigador.getFacultad());
		
		investigadorActualizado = investigadorServiceImpl.actualizarInvestigador(investigadorSeleccionado);
		
		System.out.println("El investigador actualizado es: "+ investigadorActualizado);
		
		return investigadorActualizado;
	}
	
	@DeleteMapping("/investigadores/{id}")
	public void eliminarInvestigador(@PathVariable(name="id") String id) {
		investigadorServiceImpl.eliminarInvestigador(id);
	}
}

