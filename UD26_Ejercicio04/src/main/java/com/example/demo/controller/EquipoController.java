package com.example.demo.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.demo.dto.Equipo;
import com.example.demo.service.EquipoServiceImpl;

@RestController
@RequestMapping("/api")
public class EquipoController {

	@Autowired
	EquipoServiceImpl equipoServiceImpl;
	
	@GetMapping("/equipos")
	public List<Equipo> listarEquipos(){
		return equipoServiceImpl.listarEquipos();
	}
	
	@PostMapping("/equipos")
	public Equipo salvarEquipo(@RequestBody Equipo equipo) {
		return equipoServiceImpl.guardarEquipo(equipo);
	}
	
	@GetMapping("/equipos/{id}")
	public Equipo equipoXID(@PathVariable(name="id") char id) {
		Equipo equipoXId = new Equipo();
		
		equipoXId = equipoServiceImpl.equipoXID(id);
		
		System.out.println("Equipo XID: " + equipoXId);
		
		return equipoXId;
	}
	
	@PutMapping("/equipos/{id}")
	public Equipo actualizarEquipo(@PathVariable(name="id") char id, @RequestBody Equipo equipo) {
		Equipo equipoSeleccionado = new Equipo();
		Equipo equipoActualizado = new Equipo();
		
		equipoSeleccionado = equipoServiceImpl.equipoXID(id);
		
		equipoSeleccionado.setNombre(equipo.getNombre());
		equipoSeleccionado.setFacultad(equipo.getFacultad());
		
		equipoActualizado = equipoServiceImpl.actualizarEquipo(equipoSeleccionado);
		
		System.out.println("El equipo actualizado es: "+ equipoActualizado);
		
		return equipoActualizado;
	}
	
	@DeleteMapping("/equipos/{id}")
	public void eliminarEquipo(@PathVariable(name="id") char id) {
		equipoServiceImpl.eliminarEquipo(id);
	}
	
}
