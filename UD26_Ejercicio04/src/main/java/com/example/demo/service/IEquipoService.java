package com.example.demo.service;

import java.util.List;
import com.example.demo.dto.Equipo;

public interface IEquipoService {

	//Metodos del CRUD
	public List<Equipo> listarEquipos(); //Listar All 
	
	public Equipo guardarEquipo(Equipo equipo); //Guarda un equipo CREATE
	
	public Equipo equipoXID(char id); //Leer datos de un equipo READ
	
	public Equipo actualizarEquipo(Equipo equipo); //Actualiza datos del equipo UPDATE
	
	public void eliminarEquipo(char id); // Elimina el equipo DELETE
	
}
