package com.example.demo.service;

import java.util.List;
import com.example.demo.dto.Reserva;

public interface IReservaService {

	//Metodos del CRUD
	public List<Reserva> listarReservas();  //Listar All 
	
	public Reserva guardarReserva(Reserva reserva); //Guarda una reserva CREATE
	
	public Reserva reservaXID(int id); //Leer datos de una reserva READ
	
	public Reserva actualizarReserva(Reserva reserva); //Actualiza datos de la reserva UPDATE
	
	public void eliminarReserva(int id); // Elimina la reserva DELETE

	
}

