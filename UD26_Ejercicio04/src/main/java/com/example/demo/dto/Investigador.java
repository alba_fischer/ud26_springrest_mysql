package com.example.demo.dto;

import java.util.List;
import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="investigadores")
public class Investigador {


	@Id
	@Column(name="dni")
	private String id;
	
	@Column(name="nom_apels")
	private String nombre;
	
	@ManyToOne
	@JoinColumn(name="facultad")
	Facultad facultad;
	
	@OneToMany
	@JoinColumn(name="dni")
	private List<Reserva> reserva;
	
	public Investigador() {}

	public Investigador(String id, String nombre, Facultad facultad, List<Reserva> reserva) {
		this.id = id;
		this.nombre = nombre;
		this.facultad = facultad;
		this.reserva = reserva;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Facultad getFacultad() {
		return facultad;
	}

	public void setFacultad(Facultad facultad) {
		this.facultad = facultad;
	}

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "reserva")
	public List<Reserva> getReserva() {
		return reserva;
	}

	public void setReserva(List<Reserva> reserva) {
		this.reserva = reserva;
	}
	
}
