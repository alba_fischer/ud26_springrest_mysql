package com.example.demo.service;

import java.util.List;
import com.example.demo.dto.Maquina;

public interface IMaquinaService {

	//Metodos del CRUD
	public List<Maquina> listarMaquinas(); //Listar All 
	
	public Maquina guardarMaquina(Maquina maquina);	//Guarda una maquina CREATE
	
	public Maquina maquinaXID(int id); //Leer datos de una maquina READ
	
	public Maquina actualizarMaquina(Maquina maquina); //Actualiza datos de la maquina UPDATE
	
	public void eliminarMaquina(int id);// Elimina la maquina DELETE

	
}
