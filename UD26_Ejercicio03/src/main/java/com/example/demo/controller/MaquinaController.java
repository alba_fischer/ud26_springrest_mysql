package com.example.demo.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.demo.dto.Maquina;
import com.example.demo.service.MaquinaServiceImpl;

@RestController
@RequestMapping("/api")
public class MaquinaController {


	@Autowired
	MaquinaServiceImpl maquinaServiceImpl;
	
	@GetMapping("/maquinas")
	public List<Maquina> listarMaquinas(){
		return maquinaServiceImpl.listarMaquinas();
	}
	
	
	@PostMapping("/maquinas")
	public Maquina salvarMaquina(@RequestBody Maquina maquina) {
		
		return maquinaServiceImpl.guardarMaquina(maquina);
	}
	
	
	@GetMapping("/maquinas/{id}")
	public Maquina maquinaXID(@PathVariable(name="id") int id) {
		
		Maquina Maquina_xid= new Maquina();
		
		Maquina_xid=maquinaServiceImpl.maquinaXID(id);
		
		System.out.println("Maquina XID: "+Maquina_xid);
		
		return Maquina_xid;
	}
	
	@PutMapping("/maquinas/{id}")
	public Maquina actualizarMaquina(@PathVariable(name="id")int id,@RequestBody Maquina maquina) {
		
		Maquina Maquina_seleccionado= new Maquina();
		Maquina Maquina_actualizado= new Maquina();
		
		Maquina_seleccionado= maquinaServiceImpl.maquinaXID(id);
		
		Maquina_seleccionado.setPiso(maquina.getPiso());
		
		Maquina_actualizado = maquinaServiceImpl.actualizarMaquina(Maquina_seleccionado);
		
		System.out.println("La maquina actualizada es: "+ Maquina_actualizado);
		
		return Maquina_actualizado;
	}
	
	@DeleteMapping("/maquinas/{id}")
	public void eleiminarMaquina(@PathVariable(name="id")int id) {
		maquinaServiceImpl.eliminarMaquina(id);
	}

	
}
