package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ud26Ejercicio03Application {

	public static void main(String[] args) {
		SpringApplication.run(Ud26Ejercicio03Application.class, args);
	}

}
