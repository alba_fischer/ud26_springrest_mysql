CREATE DATABASE IF NOT EXISTS `los_grandes_almacenes`;
USE `los_grandes_almacenes`;

DROP TABLE IF EXISTS `venta`;
DROP TABLE IF EXISTS `maquinas_registradoras`;
DROP TABLE IF EXISTS `productos`;
DROP TABLE IF EXISTS `cajeros`;

CREATE TABLE `cajeros` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `nom_apels` varchar(255) DEFAULT NULL
);

INSERT INTO `cajeros` (nom_apels) VALUES ('Arnau Castillo'),('Alba Carles'),('Esther Gras'),('Jaume Balada'),('Mariona Roca');

CREATE TABLE `maquinas_registradoras` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `piso` int DEFAULT NULL
);

INSERT INTO `maquinas_registradoras` (piso) VALUES (1),(2),(3),(4),(5);

CREATE TABLE `productos` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `nombre` varchar(100) DEFAULT NULL,
  `precio` int DEFAULT NULL
);

INSERT INTO `productos` (nombre, precio) VALUES ('Leche',3),('Queso',5),('Nocilla',3),('Pizza',4),('Jamon',1);

CREATE TABLE `venta` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `cajero` int NOT NULL,
  `maquina` int NOT NULL,
  `producto` int NOT NULL,
  FOREIGN KEY (`cajero`) REFERENCES `cajeros` (`id`),
  FOREIGN KEY (`maquina`) REFERENCES `maquinas_registradoras` (`id`),
  FOREIGN KEY (`producto`) REFERENCES `productos` (`id`)
);

INSERT INTO `venta` (cajero, maquina, producto) VALUES (1,1,1),(2,2,2),(3,3,3),(4,4,4),(5,5,5);

